package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.service.IProjectTaskService;
import ru.tsc.golovina.tm.exception.empty.EmptyIdException;
import ru.tsc.golovina.tm.exception.empty.EmptyNameException;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull final ITaskRepository taskRepository,
                              @NotNull final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public void bindTaskById(@NotNull final String userId, @Nullable final String projectId,
                             @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return;
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void unbindTaskById(@NotNull final String userId, @Nullable final String projectId,
                               @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId) || !taskRepository.existsById(taskId)) return;
        taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public void removeById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(projectId);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(projectId);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null) throw new EmptyNameException();
        @NotNull final Project project = projectRepository.findByName(userId, name);
        @NotNull final String projectId = project.getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(projectId);
    }

}
