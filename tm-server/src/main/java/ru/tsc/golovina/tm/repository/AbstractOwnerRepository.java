package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IOwnerRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @NotNull
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);

    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull List<String> entityUserId = list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        entityUserId.forEach(list::remove);
    }

    @NotNull
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.of(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final Optional<E> ent = Optional.ofNullable(findById(userId, entity.getId()));
        ent.ifPresent(this::remove);
    }

    @NotNull
    public Integer getSize(@NotNull final String userId) {
        @NotNull final List<E> entities = findAll(userId);
        return entities.size();
    }

    @Override
    public boolean existsByName(@Nullable String userId, @NotNull String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public @NotNull E findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public @Nullable E startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public @NotNull E startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final E entity = findByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public @NotNull E startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public @Nullable E finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public @NotNull E finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public @NotNull E finishByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(Status.COMPLETED);
        return entity;
    }

    @Override
    public @Nullable E changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    @Override
    public @NotNull E changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(status);
        return entity;
    }

    @Override
    public @NotNull E changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(status);
        return entity;
    }

    @NotNull
    @Override
    public E removeByName(@Nullable String userId, @Nullable String name) {
        @NotNull final E entity = findByName(userId, name);
        list.remove(entity);
        return entity;
    }

}
