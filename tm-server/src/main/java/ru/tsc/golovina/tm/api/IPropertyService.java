package ru.tsc.golovina.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}
