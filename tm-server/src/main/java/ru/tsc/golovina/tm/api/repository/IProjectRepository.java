package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

}
