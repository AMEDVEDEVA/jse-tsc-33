package ru.tsc.golovina.tm.component;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner implements Runnable{

    private final int interval = 3;

    @NotNull
    private final String PATH_NAME = "./";

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(bootstrap.getCommandService().getListCommandArg());
        es.scheduleWithFixedDelay(this, interval, interval, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        @NotNull final File file = new File(PATH_NAME);
        Arrays.stream(file.listFiles())
                .filter(File::isFile).collect(Collectors.toList())
                .stream()
                .filter(item -> commands.contains(item.getName()))
                .forEach(item -> {bootstrap.runCommand(item.getName()); item.delete();});

    }

}
