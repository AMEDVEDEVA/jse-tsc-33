package ru.tsc.golovina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "project-start-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter index");
        @Nullable final Integer index = TerminalUtil.nextNumber();
        serviceLocator.getProjectEndpoint().startProjectByIndex(session, index);
    }

}
