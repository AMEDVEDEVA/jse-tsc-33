package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.endpoint.*;

public interface IEndpointLocator {

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    AdminDataEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ISessionService getSessionService();

}
