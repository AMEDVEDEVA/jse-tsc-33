package ru.tsc.golovina.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.golovina.tm.endpoint.*;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.exception.AbstractException;
import ru.tsc.golovina.tm.model.Project;

public class ProjectEndpointTest {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @Nullable
    private String projectId;

    @NotNull
    private Project project;

    @NotNull
    private Session session;

    public ProjectEndpointTest() {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Before
    public void initializeTest() throws AbstractException {
        session = sessionEndpoint.openSession("user", "user");
        project = projectEndpoint.createProject(session, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllProject() throws AbstractException {
        Assert.assertEquals(1, projectEndpoint.findProjectAll(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProject() throws AbstractException {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, projectId));
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findProjectByName(session, projectName));
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(session, 0));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdProject() throws AbstractException {
        Assert.assertNotNull(projectId);
        removeProject(projectId);
        Assert.assertFalse(
                projectEndpoint
                        .findProjectAll(session)
                        .stream()
                        .anyMatch(p -> p.getId().equals(projectId))
        );
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void createProject() throws AbstractException {
        @NotNull final String tempProjectId =
                projectEndpoint.createProject(session, "test", "test").getId();
        Assert.assertNotNull(tempProjectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, tempProjectId));
        removeProject(tempProjectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdProject() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectName";
        projectEndpoint.updateProjectById(session, projectId, newProjectName, newProjectDescription);
        @NotNull final Project updatedProject = projectEndpoint.findProjectById(session, projectId);
        Assert.assertEquals(projectId, updatedProject.getId());
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void startByIdProject() throws AbstractException {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.startProjectById(session, projectId);
        Assert.assertEquals(projectEndpoint.findProjectById(session, projectId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishByIdProject() throws AbstractException {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.finishProjectById(session, projectId);
        Assert.assertEquals(projectEndpoint.findProjectById(session, projectId).getStatus(), Status.COMPLETED);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateStatusByIdProject() throws AbstractException {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.setProjectStatusById(session, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(projectEndpoint.finishProjectById(session, projectId).getStatus(), Status.IN_PROGRESS);
        projectEndpoint.updateProjectById(session, projectId, Status.COMPLETED);
        Assert.assertEquals(projectEndpoint.finishProjectById(session, projectId).getStatus(), Status.COMPLETED);
    }

    @After
    public void finalizeTest() throws AbstractException {
        if (projectId != null)
            removeProject(projectId);
        sessionEndpoint.closeSession(session);
    }

    @SneakyThrows
    private void removeProject(@NotNull final String id) {
        new ProjectTaskEndpointService().getProjectTaskEndpointPort().removeProjectById(session, id);
    }

}
